import * as mongodb from 'mongodb';

const MongoClient = mongodb.MongoClient;

export function connectToDb(uri: string, dbName: string, cachedDb: any) {
  if (cachedDb && cachedDb.serverConfig.isConnected()) {
    console.log('=> using cached database instance');
    return Promise.resolve(cachedDb);
  }

  return (
    MongoClient
      .connect(uri)
      .then(client => client.db(dbName))
  );
}