const path = require('path');
const slsw = require('serverless-webpack');

module.exports = {
  mode: slsw.lib.webpack.isLocal ? 'development' : 'production',
  target: 'node',

  entry: './handler.ts',

  module: {
    rules: [{
      test: /\.ts(x?)$/, loader: 'ts-loader'
    }]
  },

  resolve: {
    extensions: ['.ts', '.js']
  },

  output: {
    libraryTarget: 'commonjs',
    path: path.join(__dirname, '.serverless'),
    filename: 'handler.js'
  }
}
