import * as _ from 'lodash';
import BGG_GAME_FIELDS_MAP from './bggGameFieldSchema';

export function mapBggFields(FIELD_MAP, game) {
  let mappedGame = {};

  // log.debug('Incoming Raw Game: ', game);
  // console.log('Incoming Raw Game: ', game);

  for (let prop in FIELD_MAP) {
    if (!FIELD_MAP.hasOwnProperty(prop)) {
      return;
    }

    const config = FIELD_MAP[prop];
    let target;

    // event if the path doesn't exist don't crash
    try {
      target = config.path.split('.').reduce((gameObj, current) => {
        return gameObj[current];
      }, game);
    } catch (error) {
      // log.debug('Target Try/Catch Error: ', error);
      console.log('Target Try/Catch Error: ', error);
      continue;
    }

    switch (config.searchMethod) {
      case 'filter':
        mappedGame[prop] = _.filter(target, { [config.key]: config.field });
        break;

      case 'find':
        if (_.isArray(target)) {
          let value;
          const fieldExists = _.find(target, { [config.key]: config.field });

          if (fieldExists) {
            value = fieldExists[config.pull || 'value']
          }

          if (prop === 'is_expansion' && value === 'true') {
            value = true;
          }

          mappedGame[prop] = (value) ? value : config.defaultValue;
        } else {
          if (prop === 'bgg_rank' && isNaN(target.value)) {
            mappedGame[prop] = 0;
            continue;
          }
          mappedGame[prop] = target.value;
        }
        break;

      default:
        if (config.primative === 'number') {
          mappedGame[prop] = parseInt(target, 10);
        } else {
          mappedGame[prop] = target;
        }
    }

    if (typeof config.transform === 'function') {
      mappedGame[prop] = config.transform(mappedGame[prop]);
    }
  }

  return mappedGame;
}

export function formatIncludes(includes) {
  return (!includes) ? (
    includes.split(',')
      .reduce((obj, current) => {
        obj[current] = 1;
        return obj;
      }, {})
  ) : {};
}

export function prepThingResponse(game) {
  let gameData;
  try {
    gameData = mapBggFields(BGG_GAME_FIELDS_MAP, game);
  } catch (error) {
    return Promise.reject(error.toString());
  }
  return gameData;
}
