'use strict';

import * as _ from 'lodash';
// import * as Aws from 'aws-sdk';
import * as Bluebird from 'bluebird';
import Axios, { AxiosResponse } from 'axios';
import * as Cheerio from 'cheerio';
import { connectToDb } from '../../utils/dbUtils';
import { apiRequest } from '../../lib/bgg';
import { prepThingResponse } from '../../utils/bgg/bggUtils';

let cachedDb = null;
let currentPage = 1;
let lastPage = 1;

export function scrapeListing(event, context, callback) {
  const { start, limit } = event;

  context.callbackWaitsForEmptyEventLoop = false;

  // for statistic tracking
  currentPage = start || currentPage;
  lastPage = limit || lastPage;

  connectToDb(`mongodb://127.0.0.1:27017/tempgames`, 'tempgames', cachedDb)
    .then(db => {
      cachedDb = db;
      // return _scrape();
      return event.games;
    })
    .then(_filterExistingGames)
    .then(filteredGames => filteredGames.map(game => game.bgg_id))
    .then(_makeRequests)
    .then(newGames => {
      if (newGames.length) {
        console.log(`${newGames.length} new game(s) found`);
        return cachedDb.collection('games').insertMany(newGames);
      }

      callback(null, 'No new games to save');
    })
    .then(results => {
      callback(null, `${results.insertedCount} game(s) added to db`);
    })
    .catch(error => {
      callback(error);
    });
}

function _scrape() {
  return new Promise((resolve, reject) => {
    Axios
      .get(`https://boardgamegeek.com/browse/boardgame/page/${currentPage}`)
      .then(_parseHTML)
      .then(games => resolve(games))
      .catch(error => {
        console.log(`Error Getting: https://boardgamegeek.com/browse/boardgame/page/${currentPage}`);
        return reject(error);
      });
  });
}

function _parseHTML({ data }: AxiosResponse): any[] {
  const $ = Cheerio.load(data);
  const $rows = $('table#collectionitems #row_');

  return _.uniqBy((
    Object
      .keys($rows)
      .map(_extractGameData($, $rows))
      .filter(game => game !== false)
  ), 'bgg_id');
}

function _extractGameData($: any, $rows: any): (index: any) => any {
  return function(index) {
    try {
      const $game = $($rows[index]).find('.collection_objectname a');
      const href = $game.attr('href');

      return {
        bgg_id: href.split('/')[2],
        bgg_url: href,
        bgg_slug: href.split('/')[3],
        name: $game.text()
      };
    }
    catch (error) {
      return false;
    }
  }
}

// compare games from bgg with existing ones in our db
function _filterExistingGames(games) {
  return (
    games.length
      ? Bluebird.filter(games, _queryMongo(cachedDb.collection('games')))
      : []
  );
}

function _makeRequests(gameIds, accum = []) {
  let subsetIds = gameIds.splice(0, 30);

  return (
    apiRequest('thing', {
      id: subsetIds.join(','),
      type: 'boardgame,boardgameexpansion',
      stats: 1,
      videos: 1
    }).then(res => {
      const data = res.items.item;
      if (Array.isArray(data)) {
        return _makeRequests(gameIds, [...accum, ...data.map(prepThingResponse)]);
      }

      if (typeof data === 'object' && !_.isEmpty(data)) {
        return _makeRequests(gameIds, [...accum, prepThingResponse(data)]);
      }

      return accum;
    }).catch(error => {
      console.log('so much error: ', error); /* FIXME: Not good enough error handling... */
    })
  );
}

function _queryMongo(collection) {
  return (game) => new Promise(resolve => {
    collection
      .find({ bgg_id: game.bgg_id })
      .toArray((error, games) => resolve(!games.length));
  });
}
