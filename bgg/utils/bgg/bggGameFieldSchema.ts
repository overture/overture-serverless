export default {
  categories: {
    path: 'link',
    searchMethod: 'filter',
    key: 'type',
    field: 'boardgamecategory'
  },
  bgg_id: {
    path: 'id'
  },
  name: {
    path: 'name',
    searchMethod: 'find',
    key: 'type',
    field: 'primary'
  },
  year_published: {
    path: 'yearpublished.value',
    primative: 'number'
  },
  publishers: {
    path: 'link',
    searchMethod: 'filter',
    key: 'type',
    field: 'boardgamepublisher'
  },
  description: {
    path: 'description',
    transform: function(desc) {
      return (
        desc
          .replace()
          .replace(/&amp;|#35;|#10;/g, '')
          .replace(/ldquo;|rdquo;/g, '"')
      );
    }
  },
  image: {
    path: 'image'
  },
  player_min: {
    path: 'minplayers.value',
    primative: 'number'
  },
  player_max: {
    path: 'maxplayers.value',
    primative: 'number'
  },
  min_duration: {
    path: 'minplaytime.value',
    primative: 'number'
  },
  max_duration: {
    path: 'maxplaytime.value',
    primative: 'number'
  },
  mechanics: {
    path: 'link',
    searchMethod: 'filter',
    key: 'type',
    field: 'boardgamemechanic'
  },
  is_expansion: {
    path: 'link',
    searchMethod: 'find',
    key: 'type',
    field: 'boardgameexpansion',
    pull: 'inbound',
    defaultValue: false
  },
  expansions: {
    path: 'link',
    searchMethod: 'filter',
    key: 'type',
    field: 'boardgameexpansion'
  },
  bgg_rank: {
    path: 'statistics.ratings.ranks.rank',
    searchMethod: 'find',
    key: 'name',
    field: 'boardgame'
  },
  weight: {
    path: 'statistics.ratings.averageweight.value'
  }
};
